<div class="grid_eventos">
  <div class="container_eventos">
    <div class="create_modulo">
      <h4>Eventos</h4>
      <button type="submit">
        <i class="icon-edit"></i>
      </button>
    </div>
  </div>
  <div class="container_titulo">
    <p>Titulo del evento</p>
  </div>
  <div class="container_desc">
    <p>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </p>
  </div>
  <div class="container_record">
    <h4>Recordatorio</h4>
    <button type="submit" class="btn margin_top">Nuevo recordatorio</button>
    <div class="table_record margin_top">
      <div class="item">
        <p>Recoger notas del examen el 20/07/2019 a las 12:00</p>
        <div class="actions">
          <i class="icon-check"></i>
          <i class="icon-trash-2"></i>
        </div>
      </div>
      <div class="item">
        <p>Recoger notas del examen el 20/07/2019 a las 12:00</p>
        <div class="actions">
          <i class="icon-check"></i>
          <i class="icon-trash-2"></i>
        </div>
      </div>
      <div class="item">
        <p>Recoger notas del examen el 20/07/2019 a las 12:00</p>
        <div class="actions">
          <i class="icon-check"></i>
          <i class="icon-trash-2"></i>
        </div>
      </div>
      <div class="item">
        <p>Recoger notas del examen el 20/07/2019 a las 12:00</p>
        <div class="actions">
          <i class="icon-check"></i>
          <i class="icon-trash-2"></i>
        </div>
      </div>
    </div>
  </div>
</div>
