<?php
    require_once 'app/controllers/UserController.php';
    use Controller\UserController;
?>
<div class="container margin_top">
    <div class="alert">
        Es necesario tener una cuenta para poder disponer de las funcionalidades de Mi Horario
        <i class="i_close icon-x"></i>
    </div>
    <div class="introduction margin_top_expanded">
        <div class="container-texto">
            <div class="texto">
                <h2>¿Qué es Mi Horario?</h2>
                <p>
                    Praesent scelerisque lectus sit amet varius
                    tristique. Duis ultrices dui nec pretium
                    fermentum. Praesent ut sapien a magna
                    imperdiet vestibulum. Pellentesque nibh ligula,
                    consequat id risus sit amet, pellentesque ultrices
                    dui. Sed tempus augue accumsan blandit
                    tempus.
                </p>
            </div>
        </div>
        <img src="./resources/images/portada.svg" alt="portada">
    </div>
</div>

<div class="container margin_top_large">
    <div class="grid_prop">
        <div class="item">
            <div class="title">
                <i class="icon-check-circle"></i>
                <h4>Sencillo</h4>
            </div>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla urna sem, tincidunt in elementum vitae.
            </p>
        </div>
        <div class="item">
            <div class="title">
                <i class="icon-sliders"></i>
                <h4>Personalizable</h4>
            </div>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla urna sem, tincidunt in elementum vitae.
            </p>
        </div>
        <div class="item">
            <div class="title">
                <i class="icon-shield"></i>
                <h4>Seguro</h4>
            </div>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla urna sem, tincidunt in elementum vitae.
            </p>
        </div>
    </div>
</div>

<footer class="footer margin_top_large">
    <p>Todos los derechos reservados &copy; 2019</p>
</footer>
